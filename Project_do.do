* Econometrics Final Paper 
*Migration and toxic air; looking at south east asia
clear
cd "/Users/mbsalman/Desktop/Econometrics Final Paper/Data"
import delimited "/Users/mbsalman/Desktop/Econometrics Final Paper/Data/Data_Extract_From_World_Development_Indicators/862972c0-c303-417c-9e88-1bf84094759b_Data.csv", encoding(ISO-8859-1)
save WB.dta , replace
use "WB.dta",clear


destring populationtotalsppoptotl-pm25airpollutionmeanannualexposu, replace force

encode countrycode, generate(cc)
encode ïtime, generate(Year)
move cc countryname
move Year ïtime
drop ïtime
drop timecode
drop in 245/249 //These were just containing the series of worldbank dataset name and last updated information. 
xtset cc Year
{  //Renaming variables

rename netmigrationsmpopnetm net_migration
rename populationtotalsppoptotl population
rename gdppercapitacurrentusnygdppcapcd gdpc_nominal 
rename gdppercapitaconstant2015usnygdpp gdppc_real2015
rename hospitalbedsper1000peopleshmedbe hospital_beds
rename literacyrateadulttotalofpeopleag literacy
rename lifeexpectancyatbirthtotalyearss life_expectancy
rename mortalityrateattributedtohouseho mortality_rate_air_pollution
rename co2emissionsfromliquidfuelconsum co2_liquid_fuel
rename co2emissionsfromgaseousfuelconsu co2_gas_fuel
rename co2emissionsktenatmco2ekt co2_total
rename agriculturalmethaneemissionsthou agri_methane
rename agriculturalnitrousoxideemission agri_n2o
rename co2emissionsfromsolidfuelconsump co2_solid_fuel
rename methaneemissionsktofco2equivalen methane_total
rename hfcgasemissionsthousandmetricton hfc_gas
rename nitrousoxideemissionsthousandmet n2o_total 
rename othergreenhousegasemissionshfcpf other_greenhouse_gas
rename totalgreenhousegasemissionsktofc greenhouse_total
rename pm25airpollutionpopulationexpose pm25_pop_exposed 
rename pm25airpollutionmeanannualexposu pm25_mean_annual
}



*Imputing data for Net Migration: 
sort Year
//Net migration starts from 1962, data for 1960 and 1961 deleted 
drop in 1/8
bysort cc (Year) : replace net_migration= L.net_migration if missing(net_migration)

*Hausman fixed and random effects run it 
xtset cc Year 
xtreg net_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total, fe
estimates store fixed 
xtreg net_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total
estimates store random 
hausman fixed random 
// With a low p-value, I proceed with using fixed effects over random effects 

*Test data for stationarity 
xtunitroot fisher net_migration, dfuller lags(0)
gen diff_migration= net_migration[_n] - net_migration[_n-1]
xtunitroot fisher diff_migration, dfuller lags(0)
* First difference of net_migration is stationary 
xtset cc Year

*Regressions: 
//Pooled Cross section 
reg net_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total
//Pooled Cross section with clustered SE
reg net_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total, vce(cluster cc)
reg diff_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total, vce(cluster cc)

//Panel fixed effects 
xtreg diff_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total, fe
//Panel fixed effects with robus SE
xtreg diff_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total, fe robust
xtreg net_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total, fe robust

*Robustness 
//Checking for autocorrelation in unbalanced Panel data 
*I use Wooldridge (2002) test for autocorrelation of order 1 in panel 
findit xtserial 
xtserial net_migration pm25_pop_exposed
*Presence of autocorrelation 
*To control for autocorrelation in unbalanced panel data: Regression with Driscoll-Kraay standard errors 
ssc install xtscc 
xtscc diff_migration pm25_pop_exposed population gdppc_real2015 literacy life_expectancy hospital_beds co2_total  agri_methane agri_n2o, fe
xtscc diff_migration pm25_mean_annual population gdppc_real2015 literacy life_expectancy hospital_beds , fe 

* Graphs 
//The following graph is computed before imputing missing values in the dataset. 
xtline net_migration, overlay
*Descriptive Statistics for paper 
tabstat population gdppc_real2015 hospital_beds literacy life_expectancy net_migration co2_total pm25_pop_exposed, by(countryname)


